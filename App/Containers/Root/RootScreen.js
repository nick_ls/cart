import React, { Component } from "react";
import { View } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import ListScreen from "../List/ListScreen";
import CartScreen from "../Cart/CartScreen";
import { connect } from "react-redux";

const RootStackNavigator = createStackNavigator(
  {
    List: { screen: ListScreen },
    Cart: { screen: CartScreen }
  },
  {
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

const App = createAppContainer(RootStackNavigator);

class RootScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <App />
      </View>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(RootScreen);
