import React, { Component } from "react";
import { View, FlatList, Text, StyleSheet, Button } from "react-native";
import { connect } from "react-redux";
import * as CartActions from "../../Stores/Reducers/cart";
class CartScreen extends Component {
  static navigationOptions = {
    title: "Cart"
  };

  constructor(props) {
    super(props);
  }

  listItemRender = item => {
    const { dispatch } = this.props;
    let priceTotal = item.price * item.quantity;
    return (
      <View style={styles.cell}>
        <View>
          <Text style={styles.item}>{item.name}</Text>
          <Text style={styles.price}>
            ${item.price.toFixed(2)} x {item.quantity} = $
            {priceTotal.toFixed(2)}
          </Text>
        </View>
        <View style={styles.actions}>
          <Button
            onPress={() => {
              dispatch(CartActions.removeFromCart(item));
            }}
            title="➖"
            color="white"
            accessibilityLabel="remove item from cart"
          />
          <Text style={styles.item}>{item.quantity}</Text>
          <Button
            onPress={() => {
              dispatch(CartActions.addToCart(item));
            }}
            title="➕"
            color="white"
            accessibilityLabel="add item to cart"
          />
        </View>
      </View>
    );
  };

  render() {
    const { items = [] } = this.props;

    if (items.length == 0) {
      return (
        <View style={styles.container}>
          <Text style={styles.nothing}>Nothing here...</Text>
        </View>
      );
    }

    let cartTotalPrice = 0;
    items.map(item => {
      let itemTotalPrice = item.price * item.quantity;
      cartTotalPrice += itemTotalPrice;
    });

    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          renderItem={({ item }) => this.listItemRender(item)}
          keyExtractor={(item, index) => item.name}
          ListFooterComponent={
            <Text style={styles.nothing}>
              ${cartTotalPrice.toFixed(2)} in Total
            </Text>
          }
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({ ...state.cart });

export default connect(mapStateToProps)(CartScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22
  },
  item: {
    paddingLeft: 10,
    fontSize: 24,
    color: "white",
    fontWeight: "bold",
    height: 44
  },
  price: {
    fontSize: 20,
    color: "white",
    paddingLeft: 10,
    height: 24
  },
  cell: {
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#81D8D0"
  },
  actions: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  nothing: { textAlign: "center", fontSize: 20 }
});
