import React, { Component } from "react";
import { View, FlatList, Text, StyleSheet, Button } from "react-native";
import { connect } from "react-redux";
import * as CartActions from "../../Stores/Reducers/cart";
const rawProducts = [
  {
    name: "Sledgehammer",
    price: 125.76
  },
  {
    name: "Axe",
    price: 190.51
  },
  {
    name: "Bandsaw",
    price: 562.14
  },
  {
    name: "Chisel",
    price: 13.9
  },
  {
    name: "Hacksaw",
    price: 19.45
  }
];
class ListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "EzyShop",
      headerRight: (
        <Button
          onPress={() => {
            navigation.navigate("Cart");
          }}
          title="Cart"
          color="#81D8D0"
        />
      )
    };
  };

  constructor(props) {
    super(props);
    this.state = { products: rawProducts };
  }

  listItemRender = item => {
    const { dispatch } = this.props;
    return (
      <View style={styles.cell}>
        <View>
          <Text style={styles.item}>{item.name}</Text>
          <Text style={styles.price}>${item.price.toFixed(2)}</Text>
        </View>
        <Button
          onPress={() => {
            alert("added to cart");
            dispatch(CartActions.addToCart(item));
          }}
          title="Add"
          color="white"
          accessibilityLabel="add item to cart"
        />
      </View>
    );
  };

  render() {
    const { products } = this.state;
    return (
      <View style={styles.container}>
        <FlatList
          data={products}
          renderItem={({ item }) => this.listItemRender(item)}
          keyExtractor={(item, index) => item.name}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(ListScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22
  },
  item: {
    paddingLeft: 10,
    fontSize: 24,
    color: "white",
    fontWeight: "bold",
    height: 44
  },
  price: {
    fontSize: 20,
    color: "white",
    paddingLeft: 10,
    height: 24
  },
  cell: {
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#81D8D0"
  }
});
