const initialState = {
  items: []
};

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case "CART:ADD_TO_CART": {
      const { name, price } = action.payload;
      let nextItems;
      if (state.items.filter(item => item.name === name).length === 1) {
        nextItems = state.items.map(item => {
          if (item.name === name) {
            item.quantity++;
          }
          return item;
        });
      } else {
        nextItems = [{ name, price, quantity: 1 }, ...state.items];
      }
      let nextState = { items: nextItems };
      return nextState;
    }
    case "CART:REMOVE_FROM_CART": {
      const { name, price } = action.payload;
      let nextItems = state.items.map(item => {
        if (item.name === name) {
          item.quantity--;
        }
        return item;
      });
      let nextState = { items: nextItems.filter(item => item.quantity > 0) };
      return nextState;
    }
    default:
      return state;
  }
  return state;
}

export function addToCart(item) {
  return { type: "CART:ADD_TO_CART", payload: item };
}

export function removeFromCart(item) {
  return { type: "CART:REMOVE_FROM_CART", payload: item };
}
