import { createStore, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./Reducers";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  storage: storage,
  /**
   * Blacklist state that we do not need/want to persist
   */
  blacklist: [
    // 'auth',
  ]
};
export default () => {
  let root = combineReducers({ ...rootReducer });
  const persistedReducer = persistReducer(persistConfig, root);
  const store = createStore(persistedReducer, composeWithDevTools());
  const persistor = persistStore(store);
  return { store, persistor };
};
